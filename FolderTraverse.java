import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.Iterator;

/**
 * Created by Adrian on 26/09/2016.
 */
public class FolderTraverse {

    private final String json;

    public FolderTraverse(final String json) {
        this.json = json;
    }

    public String traverseFolder() {


        String newJson = json;
        ObjectMapper mapper = new ObjectMapper();
        try {
            JsonNode node = mapper.readTree(json);
            System.out.println("Node = " + node.toString());



            recursive(node, "");
        } catch (IOException e) {
            e.printStackTrace();
        }




        return newJson;

    }

    public void traverse(String json) {

    }

    public void recursive(JsonNode node, String parentName) {
        for (JsonNode jsonNode : node) {

            if (jsonNode.isValueNode()) {
                System.out.println(jsonNode.asText() + " parent: " + parentName);
                parentName = jsonNode.asText();
            }

            if (jsonNode.isArray() || jsonNode.isObject())
                recursive(jsonNode, parentName);

        }
    }
}
