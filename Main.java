import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;

/**
 * Created by Adrian on 26/09/2016.
 */
public class Main {

    public static void main(String[] args) {
        System.out.println("Main method");
        String jsonString = "[{\"folder_name\":\"root\",\"nestedFolders\":[{\"folder_name\":\"dog\",\"nestedFolders\":[{\"folder_name\":\"big\",\"nestedFolders\":[]},{\"folder_name\":\"small\",\"nestedFolders\":[]}]},{\"folder_name\":\"cat\",\"nestedFolders\":[{\"folder_name\":\"pink\",\"nestedFolders\":[]},{\"folder_name\":\"black\",\"nestedFolders\":[]}]}]}]";
        System.out.println("jsonString =" + jsonString);
        JSONArray jsonArray = new JSONArray(jsonString);
        JsonArrayTraverse.traverse(jsonArray);
     /*   FolderTraverse folderTraverse = new FolderTraverse(jsonString);
        folderTraverse.traverseFolder();*/
    }


}
