import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;

/**
 * Created by Adrian on 03/10/2016.
 */
public class JsonArrayTraverse {
    public static void traverse(JSONArray jsonArray) {
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject json = (JSONObject) jsonArray.get(i);
            ObjectMapper mapper = new ObjectMapper();
            try {
                JsonNode node = mapper.readTree(json.toString());
                System.out.println("Node = " + node.toString());



                recursive(node, "");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private static void recursive(JsonNode node, String parentName) {

        for (JsonNode jsonNode : node) {
            if (jsonNode.isValueNode()) {
                System.out.println(jsonNode.asText() + " parent: " + parentName);
                parentName = jsonNode.asText();
            }
        }

        for (JsonNode jsonNode : node) {
            if (jsonNode.isArray() || jsonNode.isObject())
                recursive(jsonNode, parentName);
        }

    }
}
